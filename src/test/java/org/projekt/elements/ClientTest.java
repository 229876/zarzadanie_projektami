package org.projekt.elements;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.projekt.elements.Address;
import org.projekt.elements.Client;

public class ClientTest {

    @Test
    public void constructorTest() {
        Address address = new Address("Warszawa", "Pilsudskiego", "23");
        Client client = new Client("123456789" , "Patryk", "Nowacki", "987654321", address);
        Assertions.assertEquals(client.getFirstName(), "Patryk");
        Assertions.assertEquals(client.getLastName(), "Nowacki");
        Assertions.assertEquals(client.getPersonalID(), "123456789");
        Assertions.assertEquals(client.getPhoneNumber(), "987654321");
        Assertions.assertEquals(client.getAddress(), address);
    }

    @Test
    public void setterTest() {
        Address address = new Address("Warszawa", "Pilsudskiego", "23");
        Client client = new Client("123456789", "Patryk", "Nowacki", "987654321", address);
        client.setFirstName("Piotrek");
        Assertions.assertEquals(client.getFirstName(), "Piotrek");
        client.setLastName("Lisicki");
        Assertions.assertEquals(client.getLastName(), "Lisicki");
        client.setPhoneNumber("666333111");
        Assertions.assertEquals(client.getPhoneNumber(), "666333111");
    }
}
