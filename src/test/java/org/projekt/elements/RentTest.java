package org.projekt.elements;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Calendar;
import java.util.Date;

class RentTest {

    private final String personalID = "123456789";
    private String firstName = "Piotr";
    private String lastName = "Nowacki";
    private String phoneNumber = "530231254";

    private String city = "Nowe";
    private String street = "Szwejki";
    private String houseNumber = "11";

    private String mark = "Fiat";
    private String model = "Brava";
    private String color = "blue";
    private int carPrice = 100;
    private String registrationNumber = "JG 1234";

    private Client client;
    private Address address;
    private Vehicle vehicle;

    Calendar c = Calendar.getInstance();

    public RentTest() {
        address = new Address(city, street, houseNumber);
        client = new Client(personalID, firstName, lastName, phoneNumber, address);
        vehicle = new Vehicle(carPrice, mark, model, color);
    }

    @Test
    public void differentUUIDTest() {
        Rent rent1 = new Rent(client, vehicle);
        Rent rent2 = new Rent(client, vehicle);

        Assertions.assertNotEquals(rent1.getRentID(), rent2.getRentID());
    }

    @Test
    public void getFinalCost_1h() {
        Rent rent = new Rent(client, vehicle);

        Date future_1h = new Date(new Date().getTime() + (60 * 60 * 1000));
        rent.endRent(future_1h);

        Assertions.assertEquals(rent.getFinalCost(), carPrice);
    }

    @Test
    public void getFinalCost_24h() {
        Rent rent = new Rent(client, vehicle);

        Date future_24h = new Date(new Date().getTime() + (60 * 60 * 1000 * 24));
        rent.endRent(future_24h);

        Assertions.assertEquals(carPrice * 2, rent.getFinalCost());
    }

    @Test
    public void getFinalCost_25h() {
        Rent rent = new Rent(client, vehicle);

        Date future_25h = new Date(new Date().getTime() + (60 * 60 * 1000 * 25));
        rent.endRent(future_25h);

        Assertions.assertEquals(carPrice * 2, rent.getFinalCost());
    }
}