package org.projekt.elements;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class VehicleTest {

    @Test
    public void constructorTest() {
        Vehicle vehicle = new Vehicle(100, "BMW", "M3", "niebieski");
        Assertions.assertEquals(vehicle.getMark(), "BMW");
        Assertions.assertEquals(vehicle.getModel(), "M3");
        Assertions.assertEquals(vehicle.getPrice(), 100);
        Assertions.assertEquals(vehicle.getColor(), "niebieski");
    }
}
