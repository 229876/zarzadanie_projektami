package org.projekt.elements;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.projekt.elements.Address;

public class AdressTest {

    @Test
    public void constructor() {
        Address address = new Address("Warszawa", "Pilsudskiego", "23");
        Assertions.assertEquals(address.getCity(), "Warszawa");
        Assertions.assertEquals(address.getStreet(), "Pilsudskiego");
        Assertions.assertEquals(address.getHouseNumber(), "23");
        // System.out.println(address.toString());
    }

    @Test
    public void setterTest() {
        Address address = new Address("Warszawa", "Pilsudskiego", "23");
        address.setAddress("Lodz", "Wigury", "15");
        Assertions.assertEquals(address.getCity(), "Lodz");
        Assertions.assertEquals(address.getStreet(), "Wigury");
        Assertions.assertEquals(address.getHouseNumber(), "15");
        // System.out.println(address.toString());
    }

}
