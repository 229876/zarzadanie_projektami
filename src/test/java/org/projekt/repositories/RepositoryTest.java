package org.projekt.repositories;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.projekt.Repository;
import org.projekt.elements.Address;
import org.projekt.elements.Client;

class RepositoryTest {

    String firstName = "Jan";
    String lastName = "Nowak";
    String personalID = "8905163481";
    String phoneNumber = "";

    String city = "Nowe";
    String street = "Szwejki";
    String houseNumber = "11";

    Address address;
    Client client;

    public RepositoryTest() {
        address = new Address(city, street, houseNumber);
        client = new Client(firstName, lastName, personalID, phoneNumber, address);
    }

    @Test
    public void addTest() {
        Repository<Client> cRepo = new Repository<>();

        Assertions.assertTrue(cRepo.add(client));
        Assertions.assertEquals(cRepo.size(), 1);
    }

    @Test
    public void removeTest() {
        Repository<Client> cRepo = new Repository<>();

        cRepo.add(client);

        Assertions.assertTrue(cRepo.remove(client));
        Assertions.assertEquals(cRepo.size(), 0);
        Assertions.assertFalse(cRepo.remove(client));
    }

    @Test
    public void getTest() {
        Repository<Client> cRepo = new Repository<>();

        cRepo.add(client);

        Assertions.assertEquals(cRepo.get(0).getPersonalID(), personalID);
    }
}