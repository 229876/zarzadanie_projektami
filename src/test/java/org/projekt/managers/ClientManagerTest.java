package org.projekt.managers;


import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.projekt.elements.Client;

import java.util.List;

class ClientManagerTest {

    String firstName = "Jan";
    String lastName = "Nowak";
    String personalID = "8905163481";
    String phoneNumber = "";

    String city = "Nowe";
    String street = "Szwejki";
    String houseNumber = "11";

    @Test
    void add_get_ClientTest() {
        ClientManager manager = new ClientManager();

        Assertions.assertTrue(manager.addClient(firstName, lastName, personalID, phoneNumber, city, street, houseNumber));

        Assertions.assertEquals(manager.getClientByPersonalID(personalID).getFirstName(), firstName);

        Assertions.assertFalse(manager.addClient(firstName, lastName, personalID, phoneNumber, city, street, houseNumber));

        Assertions.assertEquals(manager.getClientsCount(), 1);
    }

    @Test
    void removeClientTest() {
        ClientManager manager = new ClientManager();

        manager.addClient(firstName, lastName, personalID, phoneNumber, city, street, houseNumber);

        manager.getClientByPersonalID(personalID);
        Assertions.assertNotNull(manager.getClientByPersonalID(personalID));

        Assertions.assertTrue(manager.removeClient(personalID));

        Assertions.assertNull(manager.getClientByPersonalID(personalID));

        Assertions.assertEquals(manager.getClientsCount(), 0);
    }

    @Test
    void getAllTest() {
        ClientManager manager = new ClientManager();

        manager.addClient(firstName, lastName, personalID, phoneNumber, city, street, houseNumber);
        manager.addClient("firstName", "lastName", "personalID", "phoneNumber", "city", "street", "houseNumber");

        List<Client> list = manager.getAllClients();

        Assertions.assertEquals(list.get(0).getPersonalID(), personalID);
        Assertions.assertEquals(list.get(1).getPersonalID(), "personalID");
    }
}