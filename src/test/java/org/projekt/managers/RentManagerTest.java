package org.projekt.managers;

import org.junit.jupiter.api.Test;
import org.projekt.elements.Address;
import org.projekt.elements.Client;
import org.projekt.elements.Vehicle;

import java.util.Calendar;

class RentManagerTest {

    private final String personalID = "123456789";
    private String firstName = "Piotr";
    private String lastName = "Nowacki";
    private String phoneNumber = "530231254";

    private String city = "Nowe";
    private String street = "Szwejki";
    private String houseNumber = "11";

    private int carPrice = 100;
    private String registrationNumber = "JG 1234";
    private String mark = "Fiat";
    private String model = "Brava";
    private String color = "blue";

    private Client client;
    private Address address;
    private Vehicle vehicle;

    Calendar c = Calendar.getInstance();

    public RentManagerTest() {
        address = new Address(city, street, houseNumber);
        client = new Client(personalID, firstName, lastName, phoneNumber, address);
        vehicle = new Vehicle(carPrice, mark, model, color);
    }

    @Test
    public void test() {
        RentManager manager = new RentManager();

    }
}