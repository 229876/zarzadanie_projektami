package org.projekt.managers;

import org.projekt.elements.Vehicle;
import org.projekt.Repository;

import java.util.List;

public class VehicleManager {

    private Repository<Vehicle> vRepo;

    public VehicleManager(Repository<Vehicle> vRepo) {
        this.vRepo = vRepo;
    }

    public VehicleManager() {
        this.vRepo = new Repository<>();
    }

    public boolean addVehicle(String mark, String model, int price, String color) {
        return vRepo.add(new Vehicle(price, mark, model, color));
    }

    public boolean removeVehicle(Vehicle vehicle) {
        return vRepo.remove(vehicle);
    }

    public List<Vehicle> getAllVehicles () {
        return vRepo.getAll();
    }
}
