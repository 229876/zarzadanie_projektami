package org.projekt.database;

public interface Dao<O> extends AutoCloseable {
    O read();

    void write(O obj);
}
