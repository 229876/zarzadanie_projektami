package org.projekt.database;

import lombok.Setter;
import org.projekt.elements.Address;
import org.projekt.elements.Client;
import org.projekt.elements.Vehicle;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class JdbcVehicleDao {

    private static String URL = "jdbc:postgresql://localhost/pzp_database";
    private static String USERNAME = "postgres";
    private static String PASSWORD = "123123";

    public static Vehicle read(String searched_mark, String searched_model, String searched_color) {

        try {
            Connection connection;
            connection = DriverManager.getConnection(URL, USERNAME, PASSWORD);
            connection.setAutoCommit(false);

            int price;
            final String mark;
            final String model;
            final String color;

            String statement = "SELECT * FROM vehicles_table WHERE mark=? AND model=? AND color=?";
            try (PreparedStatement ps = connection.prepareStatement(statement)) {
                ps.setString(1, searched_mark);
                ps.setString(2, searched_model);
                ps.setString(3, searched_color);
                ResultSet rs = ps.executeQuery();

                if (rs.next()) {
                    price = rs.getInt("price");
                    mark = rs.getString("mark");
                    model = rs.getString("model");
                    color = rs.getString("color");

                    connection.close();

                    return new Vehicle(price, mark, model, color);
                }

            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return null;
    }

    public static boolean write(Vehicle vehicle) {

        try {
            Connection connection;
            connection = DriverManager.getConnection(URL, USERNAME, PASSWORD);
            connection.setAutoCommit(false);

            String statement =
                    "INSERT INTO vehicles_table (price, mark, model, color) VALUES (?,?,?,?)";

            try (PreparedStatement ps = connection.prepareStatement(statement)) {
                ps.setInt(1, vehicle.getPrice());
                ps.setString(2, vehicle.getMark());
                ps.setString(3, vehicle.getModel());
                ps.setString(4, vehicle.getColor());

                ps.executeUpdate();

                connection.commit();
                connection.close();
                return true;
            }

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        return false;
    }

    public static boolean remove(String searched_mark, String searched_model, String searched_color) {
        try {
            Connection connection;
            connection = DriverManager.getConnection(URL, USERNAME, PASSWORD);
            connection.setAutoCommit(false);

            String statement = "DELETE FROM vehicles_table WHERE mark=(?) AND model=(?) AND color=(?)";
            try (PreparedStatement ps = connection.prepareStatement(statement)) {
                ps.setString(1, searched_mark);
                ps.setString(2, searched_model);
                ps.setString(3, searched_color);

                ps.executeUpdate();
                connection.commit();
                connection.close();
                return true;

            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        return false;
    }

    public static List<Vehicle> readAll() {
        List<Vehicle> vehicleList = new ArrayList<>();

        try {
            Connection connection;
            connection = DriverManager.getConnection(URL, USERNAME, PASSWORD);
            connection.setAutoCommit(false);

            int price;
            String mark;
            String model;
            String color;

            String statement = "SELECT * FROM vehicles_table";
            try (PreparedStatement ps = connection.prepareStatement(statement)) {
                ResultSet rs = ps.executeQuery();

                while (rs.next()) {

                    price = rs.getInt("price");
                    mark = rs.getString("mark");
                    model = rs.getString("model");
                    color = rs.getString("color");

                    vehicleList.add(new Vehicle(price, mark, model, color));
                }

                connection.close();

            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        return vehicleList;
    }
}
