package org.projekt.database;

import org.projekt.elements.Address;
import org.projekt.elements.Client;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class JdbcClientDao {

    private static String URL = "jdbc:postgresql://localhost/pzp_database";
    private static String USERNAME = "postgres";
    private static String PASSWORD = "123123";


    public static Client read(String searched_personalID) {

        Connection connection;

        try {
            connection = DriverManager.getConnection(URL, USERNAME, PASSWORD);
            connection.setAutoCommit(false);

            Client client;

            String personalID;
            String firstName;
            String lastName;
            String phoneNumber;
            String city;
            String street;
            String houseNumber;

            String statement = "SELECT * FROM clients_table WHERE personal_id=?";
            try (PreparedStatement ps = connection.prepareStatement(statement)) {
                ps.setString(1, searched_personalID);
                ResultSet rs = ps.executeQuery();

                if (rs.next()) {
                    personalID = rs.getString("personal_id");
                    firstName = rs.getString("first_name");
                    lastName = rs.getString("last_name");
                    phoneNumber = rs.getString("phone_number");
                    city = rs.getString("city");
                    street = rs.getString("street");
                    houseNumber = rs.getString("house_number");

                    client = new Client(personalID, firstName, lastName, phoneNumber, new Address(city, street, houseNumber));
                    return client;
                } else {
                    return null;
                }

            } catch (SQLException throwables) {
                throwables.printStackTrace();
                return null;
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            return null;
        }
    }

    public static boolean write(Client obj) {

        try {
            Connection connection;
            connection = DriverManager.getConnection(URL, USERNAME, PASSWORD);
            connection.setAutoCommit(false);

            String statement =
                    "INSERT INTO clients_table (personal_id, first_name, last_name, phone_number, city, street, house_number) VALUES (?,?,?,?,?,?,?)";
            try (PreparedStatement ps = connection.prepareStatement(statement)) {
                ps.setString(1, obj.getPersonalID());
                ps.setString(2, obj.getFirstName());
                ps.setString(3, obj.getLastName());
                ps.setString(4, obj.getPhoneNumber());
                ps.setString(5, obj.getAddress().getCity());
                ps.setString(6, obj.getAddress().getStreet());
                ps.setString(7, obj.getAddress().getHouseNumber());

                ps.executeUpdate();

                connection.commit();
                connection.close();
                return true;

            } catch (SQLException throwables) {
                throwables.printStackTrace();
                return false;
            }

        } catch (SQLException throwables) {
            throwables.printStackTrace();
            return false;
        }
    }

    public static List<Client> readAll() {
        List<Client> clientList = new ArrayList<>();

        Connection connection;

        try {
            connection = DriverManager.getConnection(URL, USERNAME, PASSWORD);
            connection.setAutoCommit(false);

            String personalID;
            String firstName;
            String lastName;
            String phoneNumber;
            String city;
            String street;
            String houseNumber;

            String statement = "SELECT * FROM clients_table";
            try (PreparedStatement ps = connection.prepareStatement(statement)) {
                ResultSet rs = ps.executeQuery();

                while (rs.next()) {

                    personalID = rs.getString("personal_id");
                    firstName = rs.getString("first_name");
                    lastName = rs.getString("last_name");
                    phoneNumber = rs.getString("phone_number");
                    city = rs.getString("city");
                    street = rs.getString("street");
                    houseNumber = rs.getString("house_number");

                    clientList.add(new Client(personalID, firstName, lastName, phoneNumber, new Address(city, street, houseNumber)));
                }

                connection.close();

            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return clientList;
    }

    public static boolean remove(String searched_personalID) {

        try {
            Connection connection;
            connection = DriverManager.getConnection(URL, USERNAME, PASSWORD);
            connection.setAutoCommit(false);


            String statement = "DELETE FROM clients_table WHERE personal_id=(?);";
            try (PreparedStatement ps = connection.prepareStatement(statement)) {
                ps.setString(1, searched_personalID);

                ps.executeUpdate();
                connection.commit();
                connection.close();
                return true;

            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        return false;
    }
}
