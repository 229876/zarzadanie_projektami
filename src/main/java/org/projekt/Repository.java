package org.projekt;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Repository<T> {

    private List<T> list = new ArrayList<>();

    public T get(int id) {
        if (id >= 0) {
            return list.get(id);
        }

        return null;
    }

    public List<T> getAll() {
        return Collections.unmodifiableList(list);
    }

    public boolean add(T o) {
        if (o == null) {
            return false;
        }

        list.add(o);

        return true;
    }

    public boolean remove(T o) {
        if (o == null) {
            return false;
        }

        return list.remove(o);
    }

    public int size() {
        return list.size();
    }
}