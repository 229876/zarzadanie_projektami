package org.projekt.managers;

import org.projekt.elements.Client;
import org.projekt.elements.Rent;
import org.projekt.elements.Vehicle;
import org.projekt.Repository;

import java.util.Date;
import java.util.List;
import java.util.UUID;

public class RentManager {
    Repository<Rent> rRepo;
    Repository<Rent> archive_rRepo;

    public RentManager() {
        rRepo = new Repository<>();
        archive_rRepo = new Repository<>();
    }

    public RentManager(Repository<Rent> rRepo, Repository<Rent> archive_rRepo) {
        this.rRepo = rRepo;
        this.archive_rRepo = archive_rRepo;
    }

    public Rent startRent(Client client, Vehicle vehicle) {
        if (client == null || vehicle == null) {
            return null;
        }

        Rent rent = new Rent(client, vehicle);

        rRepo.add(rent);

        return rent;
    }

    public boolean endRent(UUID uuid) {
        if (uuid == null) {
            return false;
        }

        for (int i = 0; i < rRepo.size(); i++) {
            if (rRepo.get(i).getRentID().equals(uuid)) {
                Rent rent = rRepo.get(i);
                rent.endRent(new Date());
                archive_rRepo.add(rent);
                rRepo.remove(rent);
                return true;
            }
        }

        return false;
    }

    public Rent getRentByUUID(UUID uuid) {
        if (uuid == null) {
            return null;
        }

        for (int i = 0; i < rRepo.size(); i++) {
            if (rRepo.get(i).getRentID() == uuid) {
                return rRepo.get(i);
            }
        }

        return null;
    }

    public Rent getArchiveRentByUUID(UUID uuid) {
        if (uuid == null) {
            return null;
        }

        for (int i = 0; i < rRepo.size(); i++) {
            if (archive_rRepo.get(i).getRentID() == uuid) {
                return rRepo.get(i);
            }
        }

        return null;
    }

    public List<Rent> getAllRents() {
        return rRepo.getAll();
    }

    public List<Rent> getAllArchiveRents() {
        System.out.println(archive_rRepo.size());
        return archive_rRepo.getAll();
    }

    public int getRentsCount() {
        return rRepo.size();
    }
}
