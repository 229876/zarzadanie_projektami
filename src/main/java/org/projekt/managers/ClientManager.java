package org.projekt.managers;

import org.projekt.database.JdbcClientDao;
import org.projekt.elements.Address;
import org.projekt.elements.Client;
import org.projekt.Repository;

import java.util.List;

public class ClientManager {

    private Repository<Client> cRepo;

    public ClientManager() {
        cRepo = new Repository<>();
    }

    public ClientManager(Repository<Client> cRepo) {
        this.cRepo = cRepo;
    }

    public boolean addClient(String personalID, String firstName, String lastName, String phoneNumber, String city, String street, String houseNumber) {
//        if (getClientByPersonalID(personalID) != null) {
//            return false;
//        } else {
//            return cRepo.add(new Client(personalID, firstName, lastName, phoneNumber, new Address(city, street, houseNumber)));
//        }
        if (JdbcClientDao.read(personalID) != null) {
            return false;
        } else {
            return JdbcClientDao.write(new Client(personalID, firstName, lastName, phoneNumber, new Address(city, street, houseNumber)));
        }
    }

    public boolean removeClient(String personalID) {
//        return cRepo.remove(getClientByPersonalID(personalID));
        return JdbcClientDao.remove(personalID);
    }

    public void updateClient(Client client) {
        // TODO
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public Client getClientByPersonalID(String personalID) {
//        for (int i = 0; i < cRepo.size(); i++) {
//            if (cRepo.get(i).getPersonalID().equals(personalID)) {
//                return cRepo.get(i);
//            }
//        }
//        return null;

        return JdbcClientDao.read(personalID);
    }

    public List<Client> getAllClients() {
//        return cRepo.getAll();
        return JdbcClientDao.readAll();
    }

    public int getClientsCount() {
//        return cRepo.size();
        return JdbcClientDao.readAll().size();
    }
}