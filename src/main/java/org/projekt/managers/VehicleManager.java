package org.projekt.managers;

import org.projekt.database.JdbcVehicleDao;
import org.projekt.elements.Vehicle;
import org.projekt.Repository;

import java.util.List;

public class VehicleManager {

    private Repository<Vehicle> vRepo;

    public VehicleManager() {
        this.vRepo = new Repository<>();
    }

    public VehicleManager(Repository<Vehicle> vRepo) {
        this.vRepo = vRepo;
    }

    public boolean addNewVehicle(String mark, String model, int price , String color) {
//        return vRepo.add(new Vehicle(price, mark, model, color));
        if (JdbcVehicleDao.read(mark, model, color) != null) {
            return false;
        } else {
            return JdbcVehicleDao.write(new Vehicle(price, mark, model, color));
        }
    }

    public boolean removeVehicle(String mark, String model, String color) {
//        return vRepo.remove(findVehicle(mark, model, color));
        return JdbcVehicleDao.remove(mark, model, color);
    }

    public int getVehicleCount() {
//        return vRepo.size();
        return JdbcVehicleDao.readAll().size();
    }

    public List<Vehicle> getAllVehicles() {
//        return vRepo.getAll();
        return JdbcVehicleDao.readAll();
    }

    public Vehicle findVehicle(String mark, String model, String color) {
//        List<Vehicle> list = vRepo.getAll();
//        for (Vehicle vehicle : list) {
//            if (vehicle.getMark().equals(mark) && vehicle.getModel().equals(model) && vehicle.getColor().equals(color)) {
//                return vehicle;
//            }
//        }
//        return null;
        return JdbcVehicleDao.read(mark, model, color);
    }
}
