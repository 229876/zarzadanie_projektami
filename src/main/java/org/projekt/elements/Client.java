package org.projekt.elements;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@ToString
public class Client {

    @Setter(AccessLevel.NONE)
    private final String personalID;
    private String firstName;
    private String lastName;
    private String phoneNumber;
    private Address address;
}
