package org.projekt.elements;

import lombok.Getter;
import lombok.ToString;

import java.util.Date;
import java.util.UUID;

@Getter
@ToString
public class Rent {

    private final UUID rentID;
    private final Client client;
    private final Vehicle vehicle;
    private final Date beginDate;
    private final int vehiclePrice;
    private Date endDate;
    private int finalCost = 0;

    public Rent(Client client, Vehicle vehicle) {
        this.client = client;
        this.vehicle = vehicle;
        vehiclePrice = vehicle.getPrice();
        beginDate = new Date();
        rentID = UUID.randomUUID();
    }

    public void endRent() {
        endDate = new Date();
        finalCost = vehiclePrice * getPeriod();
    }

    public void endRent(Date endDate) {
        this.endDate = endDate;
        finalCost = vehiclePrice * getPeriod();
    }

    private int getPeriod() {
        int diff = (int) (endDate.getTime() - beginDate.getTime());
        return (diff - 1) / (60 * 60 * 1000 * 24) + 1;
    }
}
