package org.projekt.elements;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@AllArgsConstructor
@ToString
public class Vehicle {

    @Setter private int price;
    private final String mark;
    private final String model;
    private final String color;
}
