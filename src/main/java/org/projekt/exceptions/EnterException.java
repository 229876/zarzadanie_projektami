package org.projekt.exceptions;

public class EnterException extends IllegalArgumentException {
    public EnterException(final String message) {
        super(message);
    }
}
