package org.projekt.gui;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import org.projekt.managers.VehicleManager;

public class newVeicleController {

    public TextField mark_text;
    public TextField model_text;
    public TextField price_text;
    public TextField color_text;


    public Button closeButton;

    @FXML
    public void readyButton_onAction(ActionEvent actionEvent) {

        VehicleManager manager = App.getVehicleManager();

        manager.addNewVehicle(
                mark_text.getText(),
                model_text.getText(),
                Integer.parseInt(price_text.getText()),
                color_text.getText()
        );

        Stage stage = (Stage) closeButton.getScene().getWindow();
        stage.close();
    }
}
