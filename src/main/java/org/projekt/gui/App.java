package org.projekt.gui;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import lombok.Getter;
import org.projekt.managers.ClientManager;
import org.projekt.managers.RentManager;
import org.projekt.managers.VehicleManager;

import java.io.IOException;

public class App extends Application {

    private static Scene scene;

    @Getter private static final RentManager rentManager = new RentManager();
    @Getter private static final ClientManager clientManager = new ClientManager();
    @Getter private static final VehicleManager vehicleManager = new VehicleManager();

    @Override
    public void start(Stage stage) throws IOException {
        scene = new Scene(loadFXML("/primary"));
        stage.setScene(scene);
        stage.show();
    }

    static void setRoot(String fxml) throws IOException {
        scene.setRoot(loadFXML(fxml));
    }

    private static Parent loadFXML(String fxml) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource(fxml + ".fxml"));
        return fxmlLoader.load();
    }

    public static void main(String[] args) {
        launch();
    }
}