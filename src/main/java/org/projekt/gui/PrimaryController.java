package org.projekt.gui;

import java.io.IOException;
import java.util.List;
import java.util.UUID;

import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import org.projekt.elements.Client;
import org.projekt.elements.Rent;
import org.projekt.elements.Vehicle;
import org.projekt.managers.ClientManager;
import org.projekt.managers.RentManager;
import org.projekt.managers.VehicleManager;

public class PrimaryController {

    public AnchorPane rent_pane;
    public AnchorPane client_pane;
    public AnchorPane vehicle_pane;

    public ListView<GridPane> rent_listView;
    public ListView<GridPane> client_listView;
    public ListView<GridPane> vehicle_listView;
    public TextField rentSearch_text;
    public TextField clientSearch_text;
    public TextField vehicleSearch_text;
    public Button rentEnd_button;
    public Button clientRemove_button;
    public Button vehicleRemove_button;
    public AnchorPane currentRent_pane;
    public AnchorPane archiveRent_pane;
    public ListView rent_listView1;
    public TextField archiveRentSearch_text;
    public RadioButton currentRent_rb;
    public ListView<GridPane> archiveRent_listView;

    private String clickedRentUUID;
    private String clickedClientPersonalID;
    private String clickedVehicleName;

    @FXML
    private void switchToSecondary() throws IOException {
        App.setRoot("secondary");
    }

    @FXML
    public void initialize() {
        rent_pane.setVisible(true);
        client_pane.setVisible(false);
        vehicle_pane.setVisible(false);
        currentRent_pane.setVisible(true);
        archiveRent_pane.setVisible(false);

        for (int i = 0; i < 20; i++) {
            String personalID = String.valueOf(i);
            App.getClientManager().addClient(personalID, "Lorem", "Ipsum", "dolor", "sit", "amlet", "consectetur");
        }

        String x = "A";
        for (int i = 0; i < 8; i++) {
            String model = x + (i + 1);
            App.getVehicleManager().addNewVehicle("BMW", model, 120, "black");
        }

        fillRentList();
        fillClientList();
        fillVehicleList();
        fillArchiveRentList();
    }

    public void fillRentList() {
        List<Rent> rentList;
        rentList = App.getRentManager().getAllRents();

        rent_listView.getItems().clear();

        for (Rent rent : rentList) {
            rent_listView.getItems().add(fillRentGridPane(rent));
        }
    }

    public void fillClientList() {
        List<Client> clientList;
        clientList = App.getClientManager().getAllClients();

        client_listView.getItems().clear();

        for (Client client : clientList) {
            client_listView.getItems().add(fillClientGridPane(client));
        }
    }

    public void fillVehicleList() {
        List<Vehicle> vehicleList;
        vehicleList = App.getVehicleManager().getAllVehicles();

        vehicle_listView.getItems().clear();

        for (Vehicle vehicle : vehicleList) {
            vehicle_listView.getItems().add(fillVehicleGridPane(vehicle));
        }
    }

    public void fillArchiveRentList() {
        List<Rent> archiveRentList;
        archiveRentList = App.getRentManager().getAllArchiveRents();

        archiveRent_listView.getItems().clear();

        for (Rent rent : archiveRentList) {
            archiveRent_listView.getItems().add(fillArchivedRentGridPane(rent));
        }
    }

    public void rentButton_onAction(ActionEvent actionEvent) {
        rent_pane.setVisible(true);
        client_pane.setVisible(false);
        vehicle_pane.setVisible(false);
        deselectButtons();
    }

    public void clientButton_onAction(ActionEvent actionEvent) {
        rent_pane.setVisible(false);
        client_pane.setVisible(true);
        vehicle_pane.setVisible(false);
        deselectButtons();
    }

    public void vehicleButton_onAction(ActionEvent actionEvent) {
        rent_pane.setVisible(false);
        client_pane.setVisible(false);
        vehicle_pane.setVisible(true);
        deselectButtons();
    }

    public void AddNewRentButton_onAction(ActionEvent actionEvent) {
        deselectButtons();
        newWindow("/newRentWindow");
        fillRentList();
    }

    public void AddNewClientButton_onAction(ActionEvent actionEvent) {
        deselectButtons();
        newWindow("/newClientWindow");
        fillClientList();
    }

    public void AddNewVehicleButton_onAction(ActionEvent actionEvent) {
        deselectButtons();
        newWindow("/newVehicleWindow");
        fillVehicleList();
    }

    public void newWindow(String windowName) {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader();
            fxmlLoader.setLocation(getClass().getResource(windowName + ".fxml"));
            Scene scene = new Scene(fxmlLoader.load(), 630, 400);
            Stage stage = new Stage();
            stage.setScene(scene);
            stage.showAndWait();
        } catch (IOException e) {
            System.out.println("błąd przy otwieraniu nowego okna: " + windowName);
            e.printStackTrace();
        }
    }

    public void rentEndButton_onAction(ActionEvent actionEvent) {
        deselectButtons();
        RentManager manager = App.getRentManager();
        manager.endRent(UUID.fromString(clickedRentUUID));
        fillRentList();
        fillArchiveRentList();
    }

    public void removeClientButton_onAction(ActionEvent actionEvent) {
        deselectButtons();
        ClientManager manager = App.getClientManager();
        manager.removeClient(clickedClientPersonalID);
        fillClientList();
    }

    public void removeVehicleButton_onAction(ActionEvent actionEvent) {
        deselectButtons();
        VehicleManager manager = App.getVehicleManager();
        String[] s = clickedVehicleName.split(", ");
        manager.removeVehicle(s[0], s[1], s[2]);
        fillVehicleList();
    }

    public void rentSearchButton_onAction(ActionEvent actionEvent) {
        deselectButtons();

        if (rentSearch_text.getText().equals("")) {
            fillRentList();
            return;
        }

        RentManager manager = App.getRentManager();
        try {
            Rent rent = manager.getRentByUUID(UUID.fromString(rentSearch_text.getText()));
            rent_listView.getItems().clear();
            rent_listView.getItems().add(fillRentGridPane(rent));
        } catch (Exception e) {
            rent_listView.getItems().clear();
        }
    }

    public void clientSearchButton_onAction(ActionEvent actionEvent) {
        deselectButtons();

        if (clientSearch_text.getText().equals("")) {
            fillClientList();
            return;
        }

        ClientManager manager = App.getClientManager();
        Client client = manager.getClientByPersonalID(clientSearch_text.getText());
        client_listView.getItems().clear();
        try {
            client_listView.getItems().add(fillClientGridPane(client));
        } catch (Exception e) {
            rent_listView.getItems().clear();
        }
    }

    public void vehicleSearchButton_OnAction(ActionEvent actionEvent) {
        deselectButtons();

        if (vehicleSearch_text.getText().equals("")) {
            fillVehicleList();
            return;
        }

        VehicleManager manager = App.getVehicleManager();
        String[] s = vehicleSearch_text.getText().split(", ");
        try {
            Vehicle vehicle = manager.findVehicle(s[0], s[1], s[2]);
            vehicle_listView.getItems().clear();
            vehicle_listView.getItems().add(fillVehicleGridPane(vehicle));
        } catch (Exception e) {
            rent_listView.getItems().clear();
        }
    }

    public void archiveRentSearchButton_onAction(ActionEvent actionEvent) {
        deselectButtons();

        if (archiveRentSearch_text.getText().equals("")) {
            fillArchiveRentList();
            return;
        }

        RentManager manager = App.getRentManager();
        try {
            Rent rent = manager.getArchiveRentByUUID(UUID.fromString(archiveRentSearch_text.getText()));
            archiveRent_listView.getItems().clear();
            archiveRent_listView.getItems().add(fillArchivedRentGridPane(rent));
        } catch (Exception e) {
            archiveRent_listView.getItems().clear();
        }

    }

    private GridPane fillRentGridPane(Rent rent) {
        GridPane pane = new GridPane();

        pane.add(new Label("kod zamówienia: "), 0, 0);
        pane.add(new Label(rent.getRentID().toString()), 1, 0);

        pane.add(new Label("Data rozpoczęcia: "), 0, 1);
        pane.add(new Label(rent.getBeginDate().toString()), 1, 1);

        pane.add(new Label("Imię klienta: "), 0, 2);
        pane.add(new Label(rent.getClient().getFirstName()), 1, 2);

        pane.add(new Label("Nazwisko klienta: "), 0, 3);
        pane.add(new Label(rent.getClient().getLastName()), 1, 3);

        pane.add(new Label("Pesel klienta: "), 0, 4);
        pane.add(new Label(rent.getClient().getPersonalID()), 1, 4);

        pane.add(new Label("Marka samochodu: "), 0, 5);
        pane.add(new Label(rent.getVehicle().getMark()), 1, 5);

        pane.add(new Label("Model samochodu: "), 0, 6);
        pane.add(new Label(rent.getVehicle().getModel()), 1, 6);

        pane.add(new Label("Cena samochodu: "), 0, 7);
        pane.add(new Label(rent.getVehicle().getPrice() + ""), 1, 7);

        pane.setStyle("-fx-border-color: black; -fx-padding: 10px");

        return pane;
    }

    private GridPane fillClientGridPane(Client client) {
        GridPane pane = new GridPane();

        pane.add(new Label("imię: "), 0, 0);
        pane.add(new Label(client.getFirstName()), 1, 0);

        pane.add(new Label("naziwsko: "), 0, 1);
        pane.add(new Label(client.getLastName()), 1, 1);

        pane.add(new Label("pesel: "), 0, 2);
        pane.add(new Label(client.getPersonalID()), 1, 2);

        pane.add(new Label("numer telefonu: "), 0, 3);
        pane.add(new Label(client.getPhoneNumber()), 1, 3);

        pane.add(new Label("Miasto: "), 0, 4);
        pane.add(new Label(client.getAddress().getCity()), 1, 4);

        pane.add(new Label("Ulica: "), 0, 5);
        pane.add(new Label(client.getAddress().getStreet()), 1, 5);

        pane.add(new Label("numer Domu: "), 0, 6);
        pane.add(new Label(client.getAddress().getHouseNumber()), 1, 6);

        pane.setStyle("-fx-border-color: black; -fx-padding: 10px");

        return  pane;
    }

    private GridPane fillVehicleGridPane(Vehicle vehicle) {
        GridPane pane = new GridPane();
        pane.add(new Label("cena: "), 0, 0);
        pane.add(new Label(vehicle.getPrice() + ""), 1, 0);

        pane.add(new Label("marka: "), 0, 1);
        pane.add(new Label(vehicle.getMark()), 1, 1);

        pane.add(new Label("model: "), 0, 2);
        pane.add(new Label(vehicle.getModel()), 1, 2);

        pane.add(new Label("kolor: "), 0, 3);
        pane.add(new Label(vehicle.getColor()), 1, 3);

        pane.setStyle("-fx-border-color: black; -fx-padding: 10px");

        return pane;
    }

    private GridPane fillArchivedRentGridPane(Rent rent) {
        GridPane pane = new GridPane();

        pane.add(new Label("kod zamówienia: "), 0, 0);
        pane.add(new Label(rent.getRentID().toString()), 1, 0);

        pane.add(new Label("Data rozpoczęcia: "), 0, 1);
        pane.add(new Label(rent.getBeginDate().toString()), 1, 1);

        pane.add(new Label("Data zakończenia: "), 0, 2);
        pane.add(new Label(rent.getEndDate().toString()), 1, 2);

        pane.add(new Label("Całkowity koszt: "), 0, 3);
        pane.add(new Label(rent.getFinalCost() + "PLN"), 1, 3);

        pane.add(new Label("Imię klienta: "), 0, 4);
        pane.add(new Label(rent.getClient().getFirstName()), 1, 4);

        pane.add(new Label("Nazwisko klienta: "), 0, 5);
        pane.add(new Label(rent.getClient().getLastName()), 1, 5);

        pane.add(new Label("Pesel klienta: "), 0, 6);
        pane.add(new Label(rent.getClient().getPersonalID()), 1, 6);

        pane.add(new Label("Marka samochodu: "), 0, 7);
        pane.add(new Label(rent.getVehicle().getMark()), 1, 7);

        pane.add(new Label("Model samochodu: "), 0, 8);
        pane.add(new Label(rent.getVehicle().getModel()), 1, 8);

        pane.add(new Label("Cena samochodu: "), 0, 9);
        pane.add(new Label(rent.getVehicle().getPrice() + ""), 1, 9);

        pane.setStyle("-fx-border-color: black; -fx-padding: 10px");

        return pane;
    }

    public void rentClick_onAction(MouseEvent mouseEvent) {
        try {
            GridPane gridPane = rent_listView.getSelectionModel().getSelectedItem();

            clickedRentUUID = ((Label) getNodeByRowColumnIndex(0, 1, gridPane)).getText();

            rentEnd_button.setDisable(false);
        } catch (Exception e) {

        }
    }

    public void clientClick_onAction(MouseEvent mouseEvent) {
        try {
            GridPane gridPane = client_listView.getSelectionModel().getSelectedItem();
            clickedClientPersonalID = ((Label) getNodeByRowColumnIndex(2, 1, gridPane)).getText();

            clientRemove_button.setDisable(false);
        } catch (Exception e) {

        }
    }

    public void vehicleClick_onAction(MouseEvent mouseEvent) {
        try {
            GridPane gridPane = vehicle_listView.getSelectionModel().getSelectedItem();
            clickedVehicleName =
                    ((Label) getNodeByRowColumnIndex(1, 1, gridPane)).getText()
                            + ", "
                            + ((Label) getNodeByRowColumnIndex(2, 1, gridPane)).getText()
                            + ", "
                            + ((Label) getNodeByRowColumnIndex(3, 1, gridPane)).getText();

            vehicleRemove_button.setDisable(false);
        } catch (Exception e) {

        }
    }

    public Node getNodeByRowColumnIndex(final int row, final int column, GridPane gridPane) {
        Node result = null;
        ObservableList<Node> childrens = gridPane.getChildren();

        for (Node node : childrens) {
            if (GridPane.getRowIndex(node) == row && GridPane.getColumnIndex(node) == column) {
                result = node;
                break;
            }
        }
        return result;
    }

    private void deselectButtons() {
        rentEnd_button.setDisable(true);
        clientRemove_button.setDisable(true);
        vehicleRemove_button.setDisable(true);
    }

    public void archive_current_rent_pane(ActionEvent actionEvent) {
        currentRent_pane.setVisible(currentRent_rb.isSelected());
        archiveRent_pane.setVisible(!currentRent_rb.isSelected());
        fillArchiveRentList();
    }
}
