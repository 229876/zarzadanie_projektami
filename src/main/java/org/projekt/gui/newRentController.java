package org.projekt.gui;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import org.projekt.elements.Client;
import org.projekt.elements.Vehicle;
import org.projekt.managers.RentManager;
import org.projekt.managers.VehicleManager;

import java.util.List;

public class newRentController {
    public TextField clientPersonalID_text;
    public ComboBox<String> vehicle_cb;
    public Button closeButton;

    @FXML
    public void initialize() {
        VehicleManager vehicleManager = App.getVehicleManager();

        List<Vehicle> vehicleList = vehicleManager.getAllVehicles();

        for (Vehicle vehicle : vehicleList) {
            String display = String.format("%s, %s, %s, %s PLN", vehicle.getMark(), vehicle.getModel(), vehicle.getColor(), vehicle.getPrice());
            vehicle_cb.getItems().add(display);
        }
    }

    public void ReadyButton_onAction(ActionEvent actionEvent) {
        RentManager rentManager = App.getRentManager();

        Client client = App.getClientManager().getClientByPersonalID(clientPersonalID_text.getText());

        String[] s = vehicle_cb.getSelectionModel().getSelectedItem().split(", ");
        Vehicle vehicle = App.getVehicleManager().findVehicle(s[0], s[1], s[2]);

        rentManager.startRent(client, vehicle);

        Stage stage = (Stage) closeButton.getScene().getWindow();
        stage.close();
    }
}
