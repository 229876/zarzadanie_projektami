package org.projekt.gui;

import javafx.event.ActionEvent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import org.projekt.managers.ClientManager;

public class newClientController {
    public TextField firstName_text;
    public TextField lastName_text;
    public TextField personalID_text;
    public TextField phoneNumber_text;
    public TextField street_text;
    public TextField city_text;
    public TextField houseNumber_text;
    public Button closeButton;


    public void readyButton_onAction(ActionEvent actionEvent) {
        ClientManager manager = App.getClientManager();

        boolean result = manager.addClient(
                personalID_text.getText(),
                firstName_text.getText(),
                lastName_text.getText(),
                phoneNumber_text.getText(),
                street_text.getText(),
                city_text.getText(),
                houseNumber_text.getText());

        Stage stage = (Stage) closeButton.getScene().getWindow();
        stage.close();
    }
}
